# Librerías
import os
import tensorflow as tf
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import copy

# Funciones preprocesamiento imagen para cada una de las redes
def preprocess_mobilev2(image):
  if image.shape[2] == 1:
    image = tf.image.grayscale_to_rgb(image)
  image = tf.cast(image, tf.float32)
  image = tf.image.resize(image, (224, 224))
  image = tf.keras.applications.mobilenet_v2.preprocess_input(image)
  image = image[None, ...]
  return image

def preprocess_inceptionv3(image):
  if image.shape[2] == 1:
    image = tf.image.grayscale_to_rgb(image)
  image = tf.cast(image, tf.float32)
  image = tf.image.resize(image, (299, 299))
  image = tf.keras.applications.inception_v3.preprocess_input(image)
  image = image[None, ...]
  return image

def preprocess_xception(image):
  if image.shape[2] == 1:
    image = tf.image.grayscale_to_rgb(image)
  image = tf.cast(image, tf.float32)
  image = tf.image.resize(image, (299, 299))
  image = tf.keras.applications.xception.preprocess_input(image)
  image = image[None, ...]
  return image

def preprocess_resnet50(image):
  if image.shape[2] == 1:
    image = tf.image.grayscale_to_rgb(image)
  image = tf.cast(image, tf.float32)
  image = tf.image.resize(image, (224, 224))
  image = tf.keras.applications.resnet50.preprocess_input(image)
  image = image[:, :, ::-1]
  image = image/150
  image = image[None, ...]
  return image

# Funcion para sacar la etiqueta con más probabilidad de cada red
def get_imagenet_label(probs, decode_predictions):
  return decode_predictions(probs, top=1)[0][0]