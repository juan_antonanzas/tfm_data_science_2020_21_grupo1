import tensorflow as tf
import eagerpy as ep
import numpy as np
import pandas as pd
import foolbox as fb
from foolbox import TensorFlowModel, accuracy, samples, Model
from foolbox.attacks import L2CarliniWagnerAttack
import os
from dataset_to_test import adexamples_categories
from commons import preprocess_xception, preprocess_resnet50, preprocess_mobilev2, preprocess_inceptionv3, get_imagenet_label

# Local
# directorio = "C:/Users/janto/Desktop/Master Data Science/20. TFM/REPO/tfm_data_science_2020_21_grupo1/Datasets/101_ObjectCategories/"

# Directorio relativo a la carpeta python_project de github
directorio = os.getcwd()[0:os.getcwd().rfind("\\")] + '\\Datasets\\101_ObjectCategories\\'


# Instanciación de un modelo MOBILENETV2 y copia del modelo para adaptarlo a las características del paquete foolbox para creación de ejemplos adversarios
mobilenetv2_model = tf.keras.applications.MobileNetV2(include_top=True, weights='imagenet')
mobilenetv2_model.trainable = False
fmodel: Model = TensorFlowModel(mobilenetv2_model, bounds=(0, 255))
# Creacción del dataset
mobilenetv2_df_carwag = pd.DataFrame(columns=["File", "Image Type", "Epsilon", "Category", "Predicted Label", "Accuracy", "Correct Prediction"])
decode_predictions = tf.keras.applications.mobilenet_v2.decode_predictions
# Bucle para todas las imágenes de las categorias de adexamples_categories
for example_categorie in adexamples_categories:
  for file in sorted(os.listdir(directorio+example_categorie)):
    # Selección de la imagen original
    image_path = directorio+example_categorie+"\\"+file
    image_raw = tf.io.read_file(image_path)
    image_raw = tf.image.decode_image(image_raw)
    # Predicción de la imagen original
    image = preprocess_mobilev2(image_raw)
    image_probs = mobilenetv2_model.predict(image)
    row = [file, "Original", "", example_categorie, get_imagenet_label(image_probs, decode_predictions)[1], get_imagenet_label(image_probs, decode_predictions)[2], example_categorie == get_imagenet_label(image_probs, decode_predictions)[1]]
    df_length = len(mobilenetv2_df_carwag)
    mobilenetv2_df_carwag.loc[df_length] = row
    # Etiqueta extraída para la realización del ejemplo adversario
    label = np.where(image_probs == image_probs.max())[1][0]
    label = np.array(label)
    label = tf.constant([label])
    # Ataque CarliniWagner
    attack = L2CarliniWagnerAttack()
    epsilons = [0.001, 0.01, 0.1, 0.3]
    raw_advs, clipped_advs, success = attack(fmodel, image, label, epsilons=epsilons)
    # Predicción ejemplo adversario
    for i, eps in enumerate(epsilons):
      image_probs_advs = mobilenetv2_model.predict(clipped_advs[i])
      row = [file, "Adversarial Example", eps, example_categorie, get_imagenet_label(image_probs_advs, decode_predictions)[1], get_imagenet_label(image_probs_advs, decode_predictions)[2], example_categorie == get_imagenet_label(image_probs_advs, decode_predictions)[1]]
      df_length = len(mobilenetv2_df_carwag)
      mobilenetv2_df_carwag.loc[df_length] = row




# Instanciación de un modelo INCEPTIONV3 y copia del modelo para adaptarlo a las características del paquete foolbox para creación de ejemplos adversarios
inceptionv3_model = tf.keras.applications.InceptionV3(include_top=True, weights='imagenet')
inceptionv3_model.trainable = False
fmodel: Model = TensorFlowModel(inceptionv3_model, bounds=(0, 255))
# Creacción del dataset
inceptionv3_df_carwag = pd.DataFrame(columns=["File", "Image Type", "Epsilon", "Category", "Predicted Label", "Accuracy", "Correct Prediction"])
decode_predictions = tf.keras.applications.inception_v3.decode_predictions
# Bucle para todas las imágenes de las categorias de adexamples_categories
for example_categorie in adexamples_categories:
  for file in sorted(os.listdir(directorio+example_categorie)):
    # Selección de la imagen original
    image_path = directorio+example_categorie+"\\"+file
    image_raw = tf.io.read_file(image_path)
    image_raw = tf.image.decode_image(image_raw)
    # Predicción de la imagen original
    image = preprocess_inceptionv3(image_raw)
    image_probs = inceptionv3_model.predict(image)
    row = [file, "Original", "", example_categorie, get_imagenet_label(image_probs, decode_predictions)[1], get_imagenet_label(image_probs, decode_predictions)[2], example_categorie == get_imagenet_label(image_probs, decode_predictions)[1]]
    df_length = len(inceptionv3_df_carwag)
    inceptionv3_df_carwag.loc[df_length] = row
    # Etiqueta extraída para la realización del ejemplo adversario
    label = np.where(image_probs == image_probs.max())[1][0]
    label = np.array(label)
    label = tf.constant([label])
    # Ataque CarliniWagner
    attack = L2CarliniWagnerAttack()
    epsilons = [0.001, 0.01, 0.1, 0.3]
    raw_advs, clipped_advs, success = attack(fmodel, image, label, epsilons=epsilons)
    # Predicción ejemplo adversario
    for i, eps in enumerate(epsilons):
      image_probs_advs = inceptionv3_model.predict(clipped_advs[i])
      row = [file, "Adversarial Example", eps, example_categorie, get_imagenet_label(image_probs_advs, decode_predictions)[1], get_imagenet_label(image_probs_advs, decode_predictions)[2], example_categorie == get_imagenet_label(image_probs_advs, decode_predictions)[1]]
      df_length = len(inceptionv3_df_carwag)
      inceptionv3_df_carwag.loc[df_length] = row






# Instanciación de un modelo XCEPTION y copia del modelo para adaptarlo a las características del paquete foolbox para creación de ejemplos adversarios
xception_model = tf.keras.applications.Xception(include_top=True, weights='imagenet')
xception_model.trainable = False
fmodel: Model = TensorFlowModel(xception_model, bounds=(0, 255))
# Creacción del dataset
xception_df_carwag = pd.DataFrame(columns=["File", "Image Type", "Epsilon", "Category", "Predicted Label", "Accuracy", "Correct Prediction"])
decode_predictions = tf.keras.applications.xception.decode_predictions
# Bucle para todas las imágenes de las categorias de adexamples_categories
for example_categorie in adexamples_categories:
  for file in sorted(os.listdir(directorio+example_categorie)):
    # Selección de la imagen original
    image_path = directorio+example_categorie+"\\"+file
    image_raw = tf.io.read_file(image_path)
    image_raw = tf.image.decode_image(image_raw)
    # Predicción de la imagen original
    image = preprocess_xception(image_raw)
    image_probs = xception_model.predict(image)
    row = [file, "Original", "", example_categorie, get_imagenet_label(image_probs, decode_predictions)[1], get_imagenet_label(image_probs, decode_predictions)[2], example_categorie == get_imagenet_label(image_probs, decode_predictions)[1]]
    df_length = len(xception_df_carwag)
    xception_df_carwag.loc[df_length] = row
    # Etiqueta extraída para la realización del ejemplo adversario
    label = np.where(image_probs == image_probs.max())[1][0]
    label = np.array(label)
    label = tf.constant([label])
    # Ataque CarliniWagner
    attack = L2CarliniWagnerAttack()
    epsilons = [0.001, 0.01, 0.1, 0.3]
    raw_advs, clipped_advs, success = attack(fmodel, image, label, epsilons=epsilons)
    # Predicción ejemplo adversario
    for i, eps in enumerate(epsilons):
      image_probs_advs = xception_model.predict(clipped_advs[i])
      row = [file, "Adversarial Example", eps, example_categorie, get_imagenet_label(image_probs_advs, decode_predictions)[1], get_imagenet_label(image_probs_advs, decode_predictions)[2], example_categorie == get_imagenet_label(image_probs_advs, decode_predictions)[1]]
      df_length = len(xception_df_carwag)
      xception_df_carwag.loc[df_length] = row




# Instanciación de un modelo RESNET50 y copia del modelo para adaptarlo a las características del paquete foolbox para creación de ejemplos adversarios
resnet50_model = tf.keras.applications.ResNet50(include_top=True, weights='imagenet')
resnet50_model.trainable = False
pre = dict(flip_axis=-1, mean=[104.0, 116.0, 123.0])  # RGB to BGR
fmodel: Model = TensorFlowModel(resnet50_model, bounds=(0, 255), preprocessing=pre)
fmodel = fmodel.transform_bounds((0, 1))
# Creacción del dataset
resnet50_df_carwag = pd.DataFrame(columns=["File", "Image Type", "Epsilon", "Category", "Predicted Label", "Accuracy", "Correct Prediction"])
decode_predictions = tf.keras.applications.resnet50.decode_predictions
# Bucle para todas las imágenes de las categorias de adexamples_categories
for example_categorie in adexamples_categories:
  for file in sorted(os.listdir(directorio+example_categorie)):
    # Selección de la imagen original
    image_path = directorio+example_categorie+"\\"+file
    image_raw = tf.io.read_file(image_path)
    image_raw = tf.image.decode_image(image_raw)
    # Predicción de la imagen original
    image = preprocess_resnet50(image_raw)
    image_probs = resnet50_model.predict(image)
    row = [file, "Original", "", example_categorie, get_imagenet_label(image_probs, decode_predictions)[1], get_imagenet_label(image_probs, decode_predictions)[2], example_categorie == get_imagenet_label(image_probs, decode_predictions)[1]]
    df_length = len(resnet50_df_carwag)
    resnet50_df_carwag.loc[df_length] = row
    # Etiqueta extraída para la realización del ejemplo adversario
    label = np.where(image_probs == image_probs.max())[1][0]
    label = np.array(label)
    label = tf.constant([label])
    # Ataque CarliniWagner
    attack = L2CarliniWagnerAttack()
    epsilons = [0.001, 0.01, 0.1, 0.3]
    raw_advs, clipped_advs, success = attack(fmodel, image, label, epsilons=epsilons)
    # Predicción ejemplo adversario
    for i, eps in enumerate(epsilons):
      image_probs_advs = resnet50_model.predict(clipped_advs[i])
      row = [file, "Adversarial Example", eps, example_categorie, get_imagenet_label(image_probs_advs, decode_predictions)[1], get_imagenet_label(image_probs_advs, decode_predictions)[2], example_categorie == get_imagenet_label(image_probs_advs, decode_predictions)[1]]
      df_length = len(resnet50_df_carwag)
      resnet50_df_carwag.loc[df_length] = row


# Guardado de los dataframes de resultados
# mobilenetv2_df_carwag.to_csv('dataframes_results\\mobilenetv2_df_carwag.csv', index=False)
# inceptionv3_df_carwag.to_csv('dataframes_results\\inceptionv3_df_carwag.csv', index=False)
# xception_df_carwag.to_csv('dataframes_results\\xception_df_carwag.csv', index=False)
# resnet50_df_carwag.to_csv('dataframes_results\\resnet50_df_carwag.csv', index=False)
