import tensorflow as tf
import eagerpy as ep
import numpy as np
import foolbox as fb
from foolbox import TensorFlowModel, accuracy, samples, Model
from foolbox.attacks import L2CarliniWagnerAttack
from commons import preprocess_xception, preprocess_resnet50, preprocess_mobilev2, preprocess_inceptionv3, get_imagenet_label
from foolbox.attacks import LinfPGD


# instanciación de un modelo MOBILENETV2
resnet50_model = tf.keras.applications.ResNet50(include_top=True, weights='imagenet')
resnet50_model.trainable = False
pre = dict(flip_axis=-1, mean=[104.0, 116.0, 123.0])  # RGB to BGR
fmodel: Model = TensorFlowModel(resnet50_model, bounds=(0, 255), preprocessing=pre)
fmodel = fmodel.transform_bounds((0, 1))



# Cogemos la imagen
image_path = tf.keras.utils.get_file('YellowLabradorLooking_new.jpg','https://storage.googleapis.com/download.tensorflow.org/example_images/YellowLabradorLooking_new.jpg')
image_raw = tf.io.read_file(image_path)
image_raw = tf.image.decode_image(image_raw)
# Predicción de la imagen original
decode_predictions = tf.keras.applications.resnet50.decode_predictions
image = preprocess_resnet50(image_raw)
plt.imshow(image[0])
image_probs = resnet50_model.predict(image)
get_imagenet_label(image_probs)[1]
get_imagenet_label(image_probs)[2]

# Etiqueta extraída para la realización del ejemplo adversario
label = np.where(image_probs == image_probs.max())[1][0]
label = np.array(label)
label = tf.constant([label])


# Ataque CarliniWagner
attack = L2CarliniWagnerAttack()
epsilons = [0.001, 0.01, 0.1, 0.3, 1, 30]
raw_advs, clipped_advs, success = attack(fmodel, image, label, epsilons=epsilons)

# Predicción ejemplo adversario
image_probs_advs = mobilenetv2_model.predict(clipped_advs[5])
get_imagenet_label(image_probs_advs)[1]
get_imagenet_label(image_probs_advs)[2]


fb.plot.images(clipped_advs[5])
image[0]-clipped_advs[5]
plt.imshow(clipped_advs[5][0])