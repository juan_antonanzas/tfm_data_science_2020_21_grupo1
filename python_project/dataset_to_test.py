# Librerías
from commons import preprocess_xception, preprocess_resnet50, preprocess_mobilev2, preprocess_inceptionv3
import sys
import os
import tensorflow as tf
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import copy

# Selección de las "primeras" 1000 categorías de la red
def get_imagenet_labels(probs, decode_predictions):
  return decode_predictions(probs, top=1000)[0]


def models_common_categories():
  #Cogemos una imagen cualquiera para sacar las etiquetas asociadas a cada aplicacion de keras
  image_path = tf.keras.utils.get_file('YellowLabradorLooking_new.jpg', 'https://storage.googleapis.com/download.tensorflow.org/example_images/YellowLabradorLooking_new.jpg')
  image_raw = tf.io.read_file(image_path)
  image_raw = tf.image.decode_image(image_raw)

  # Categorías MOBILENETV2
  mobilenetv2_model = tf.keras.applications.MobileNetV2(include_top=True, weights='imagenet')
  mobilenetv2_model.trainable = False
  decode_predictions = tf.keras.applications.mobilenet_v2.decode_predictions
  #Preprocesado de la imagen y Prediccion del modelo (MOBILENETV2)
  image = preprocess_mobilev2(image_raw)
  image_probs = mobilenetv2_model.predict(image)
  #Extracción de Etiquetas (MOBILENETV2)
  labels = get_imagenet_labels(image_probs, decode_predictions)
  labels = [y for (x,y,z) in labels]
  labels = pd.Series(labels, name="mobilenetv2")
  labels.sort_values(inplace=True)
  mobilenetv2 = copy.deepcopy(labels)
  mobilenetv2 = mobilenetv2.to_frame()
  mobilenetv2.value_counts()[mobilenetv2.value_counts() > 1]
  delete_maillot = mobilenetv2[mobilenetv2['mobilenetv2'] == 'maillot'].iloc[0].name
  delete_crane = mobilenetv2[mobilenetv2['mobilenetv2'] == 'crane'].iloc[0].name
  mobilenetv2.drop(labels=[delete_maillot, delete_crane], axis=0, inplace=True)

  # Categorías INCEPTIONV3
  inceptionv3_model = tf.keras.applications.InceptionV3(include_top=True, weights='imagenet')
  inceptionv3_model.trainable = False
  decode_predictions = tf.keras.applications.inception_v3.decode_predictions
  #Preprocesado de la imagen y Prediccion del modelo (INCEPTIONV3)
  image = preprocess_inceptionv3(image_raw)
  image_probs = inceptionv3_model.predict(image)
  #Extracción de Etiquetas (INCEPTIONV3)
  labels = get_imagenet_labels(image_probs, decode_predictions)
  labels = [y for (x,y,z) in labels]
  labels = pd.Series(labels, name="inceptionv3")
  labels.sort_values(inplace=True)
  inceptionv3 = copy.deepcopy(labels)
  inceptionv3 = inceptionv3.to_frame()
  inceptionv3.value_counts()[inceptionv3.value_counts() > 1]
  delete_maillot = inceptionv3[inceptionv3['inceptionv3'] == 'maillot'].iloc[0].name
  delete_crane = inceptionv3[inceptionv3['inceptionv3'] == 'crane'].iloc[0].name
  inceptionv3.drop(labels=[delete_maillot, delete_crane], axis=0, inplace=True)

  # Categorías XCEPTION
  xception_model = tf.keras.applications.Xception(include_top=True, weights='imagenet')
  xception_model.trainable = False
  decode_predictions = tf.keras.applications.xception.decode_predictions
  #Preprocesado de la imagen y Prediccion del modelo (XCEPTION)
  image = preprocess_xception(image_raw)
  image_probs = xception_model.predict(image)
  #Extracción de Etiquetas (XCEPTION)
  labels = get_imagenet_labels(image_probs, decode_predictions)
  labels = [y for (x,y,z) in labels]
  labels = pd.Series(labels, name="xception")
  labels.sort_values(inplace=True)
  xception = copy.deepcopy(labels)
  xception = xception.to_frame()
  xception.value_counts()[xception.value_counts() > 1]
  delete_maillot = xception[xception['xception'] == 'maillot'].iloc[0].name
  delete_crane = xception[xception['xception'] == 'crane'].iloc[0].name
  xception.drop(labels=[delete_maillot, delete_crane], axis=0, inplace=True)

  # Categorías RESNET50
  resnet50_model = tf.keras.applications.ResNet50(include_top=True, weights='imagenet')
  resnet50_model.trainable = False
  decode_predictions = tf.keras.applications.resnet50.decode_predictions
  #Preprocesado de la imagen y Prediccion del modelo (RESNET50)
  image = preprocess_resnet50(image_raw)
  image_probs = resnet50_model.predict(image)
  #Extracción de Etiquetas (RESNET50)
  labels = get_imagenet_labels(image_probs,decode_predictions)
  labels = [y for (x,y,z) in labels]
  labels = pd.Series(labels, name="resnet50")
  labels.sort_values(inplace=True)
  resnet50 = copy.deepcopy(labels)
  resnet50 = resnet50.to_frame()
  resnet50.value_counts()[resnet50.value_counts() > 1]
  delete_maillot = resnet50[resnet50['resnet50'] == 'maillot'].iloc[0].name
  delete_crane = resnet50[resnet50['resnet50'] == 'crane'].iloc[0].name
  resnet50.drop(labels=[delete_maillot, delete_crane], axis=0, inplace=True)

  # Categorías comunes a todas las redes
  common_categories = pd.merge(mobilenetv2, inceptionv3, left_on="mobilenetv2", right_on="inceptionv3")
  common_categories = pd.merge(common_categories, xception, left_on="mobilenetv2", right_on="xception")
  common_categories = pd.merge(common_categories, resnet50, left_on="mobilenetv2", right_on="resnet50")

  # Categorías del dataset Caltech101
  caltech101 = []
  # local
  # directorio = "C:/Users/janto/Desktop/Master Data Science/20. TFM/REPO/tfm_data_science_2020_21_grupo1/Datasets/101_ObjectCategories"
  # Directorio relativo a la carpeta python_project de github
  directorio = os.getcwd()[0:os.getcwd().rfind("\\")] + '\\Datasets\\101_ObjectCategories'
  for file in sorted(os.listdir(directorio)):
    caltech101.append(file)
  caltech101 = pd.DataFrame(caltech101, columns=['caltech101'])

  # Selección de las 4 categorías a entrenar aleatoriamente
  common_categories = pd.merge(common_categories, caltech101, left_on="mobilenetv2", right_on="caltech101")
  sample_common_categories= common_categories.sample(n=4, random_state=25)
  return sample_common_categories

sample_model_categories = models_common_categories()

adexamples_categories = list(sample_model_categories.iloc[:,1])
