# Librerías
import os
import tensorflow as tf
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import copy
from dataset_to_test import adexamples_categories
from commons import preprocess_xception, preprocess_resnet50, preprocess_mobilev2, preprocess_inceptionv3, get_imagenet_label



# Para ejecutar la predicción de las imagenes originales y ejemplos adversarios en los 4 modelos
# Directorio local
# directorio = "C:/Users/janto/Desktop/Master Data Science/20. TFM/REPO/tfm_data_science_2020_21_grupo1/Datasets/101_ObjectCategories/"
# Directorio relativo a la carpeta python_project de github
directorio = os.getcwd()[0:os.getcwd().rfind("\\")] + '\\Datasets\\101_ObjectCategories\\'
loss_object = tf.keras.losses.CategoricalCrossentropy()
epsilons = [0.001, 0.01, 0.1, 0.3]


# Predicciones para Red MobileNetV2
# Modelos y dataframe
mobilenetv2_model = tf.keras.applications.MobileNetV2(include_top=True, weights='imagenet')
mobilenetv2_model.trainable = False
mobilenetv2_df_fgsm = pd.DataFrame(columns=["File", "Image Type", "Epsilon", "Category", "Predicted Label", "Accuracy", "Correct Prediction"])
decode_predictions = tf.keras.applications.mobilenet_v2.decode_predictions

def create_adversarial_pattern_mobilenetv2(input_image, input_label):
  with tf.GradientTape() as tape:
    tape.watch(input_image)
    prediction = mobilenetv2_model(input_image)
    loss = loss_object(input_label, prediction)
  # Get the gradients of the loss w.r.t to the input image.
  gradient = tape.gradient(loss, input_image)
  # Get the sign of the gradients to create the perturbation
  signed_grad = tf.sign(gradient)
  return signed_grad

# Bucle para todas las imágenes de las categorias de adexamples_categories
for example_categorie in adexamples_categories:
  for file in sorted(os.listdir(directorio+example_categorie)):
    # Selección de la imagen original
    image_path = directorio+example_categorie+"\\"+file
    image_raw = tf.io.read_file(image_path)
    image = tf.image.decode_image(image_raw)
    image = preprocess_mobilev2(image)
    # Prediccion del modelo
    image_probs = mobilenetv2_model.predict(image)
    row = [file, "Original", "", example_categorie, get_imagenet_label(image_probs, decode_predictions)[1], get_imagenet_label(image_probs, decode_predictions)[2], example_categorie==get_imagenet_label(image_probs, decode_predictions)[1]]
    df_length = len(mobilenetv2_df_fgsm)
    mobilenetv2_df_fgsm.loc[df_length] = row
    # Creación de los ejemplos adversarios y prediccion de los mismos
    etiqueta_categoria = np.where(image_probs == image_probs.max())[1][0]
    label = tf.one_hot(etiqueta_categoria, image_probs.shape[-1])
    label = tf.reshape(label, (1, image_probs.shape[-1]))
    perturbations = create_adversarial_pattern_mobilenetv2(image, label)
    for i, eps in enumerate(epsilons):
      adv_x = image + eps*perturbations
      adv_x = tf.clip_by_value(adv_x, -1, 1)
      ad_example_probs = mobilenetv2_model.predict(adv_x)
      row = [file, "Adversarial Example", eps, example_categorie, get_imagenet_label(ad_example_probs, decode_predictions)[1], get_imagenet_label(ad_example_probs, decode_predictions)[2], example_categorie == get_imagenet_label(ad_example_probs, decode_predictions)[1]]
      df_length = len(mobilenetv2_df_fgsm)
      mobilenetv2_df_fgsm.loc[df_length] = row





# Predicciones para Red InceptionV3
# Modelos y dataframe
inceptionv3_model = tf.keras.applications.InceptionV3(include_top=True, weights='imagenet')
inceptionv3_model.trainable = False
inceptionv3_df_fgsm = pd.DataFrame(columns=["File", "Image Type", "Epsilon", "Category", "Predicted Label", "Accuracy", "Correct Prediction"])
decode_predictions = tf.keras.applications.inception_v3.decode_predictions

def create_adversarial_pattern_inceptionv3(input_image, input_label):
  with tf.GradientTape() as tape:
    tape.watch(input_image)
    prediction = inceptionv3_model(input_image)
    loss = loss_object(input_label, prediction)
  # Get the gradients of the loss w.r.t to the input image.
  gradient = tape.gradient(loss, input_image)
  # Get the sign of the gradients to create the perturbation
  signed_grad = tf.sign(gradient)
  return signed_grad

# Bucle para todas las imágenes de las categorias de adexamples_categories
for example_categorie in adexamples_categories:
  for file in sorted(os.listdir(directorio+example_categorie)):
    # Selección de la imagen original
    image_path = directorio+example_categorie+"\\"+file
    image_raw = tf.io.read_file(image_path)
    image = tf.image.decode_image(image_raw)
    image = preprocess_inceptionv3(image)
    # Prediccion del modelo
    image_probs = inceptionv3_model.predict(image)
    row = [file, "Original", "", example_categorie, get_imagenet_label(image_probs, decode_predictions)[1], get_imagenet_label(image_probs, decode_predictions)[2], example_categorie==get_imagenet_label(image_probs, decode_predictions)[1]]
    df_length = len(inceptionv3_df_fgsm)
    inceptionv3_df_fgsm.loc[df_length] = row
    # Creación de los ejemplos adversarios y prediccion de los mismos
    etiqueta_categoria = np.where(image_probs == image_probs.max())[1][0]
    label = tf.one_hot(etiqueta_categoria, image_probs.shape[-1])
    label = tf.reshape(label, (1, image_probs.shape[-1]))
    perturbations = create_adversarial_pattern_inceptionv3(image, label)
    for i, eps in enumerate(epsilons):
      adv_x = image + eps*perturbations
      adv_x = tf.clip_by_value(adv_x, -1, 1)
      ad_example_probs = inceptionv3_model.predict(adv_x)
      row = [file, "Adversarial Example", eps, example_categorie, get_imagenet_label(ad_example_probs, decode_predictions)[1], get_imagenet_label(ad_example_probs, decode_predictions)[2], example_categorie == get_imagenet_label(ad_example_probs, decode_predictions)[1]]
      df_length = len(inceptionv3_df_fgsm)
      inceptionv3_df_fgsm.loc[df_length] = row



# Predicciones para Red Xception
# Modelos y dataframe
xception_model = tf.keras.applications.Xception(include_top=True, weights='imagenet')
xception_model.trainable = False
xception_df_fgsm = pd.DataFrame(columns=["File", "Image Type", "Epsilon", "Category", "Predicted Label", "Accuracy", "Correct Prediction"])
decode_predictions = tf.keras.applications.xception.decode_predictions

def create_adversarial_pattern_xception(input_image, input_label):
  with tf.GradientTape() as tape:
    tape.watch(input_image)
    prediction = xception_model(input_image)
    loss = loss_object(input_label, prediction)
  # Get the gradients of the loss w.r.t to the input image.
  gradient = tape.gradient(loss, input_image)
  # Get the sign of the gradients to create the perturbation
  signed_grad = tf.sign(gradient)
  return signed_grad

# Bucle para todas las imágenes de las categorias de adexamples_categories
for example_categorie in adexamples_categories:
  for file in sorted(os.listdir(directorio+example_categorie)):
    # Selección de la imagen original
    image_path = directorio+example_categorie+"\\"+file
    image_raw = tf.io.read_file(image_path)
    image = tf.image.decode_image(image_raw)
    image = preprocess_xception(image)
    # Prediccion del modelo
    image_probs = xception_model.predict(image)
    row = [file, "Original", "", example_categorie, get_imagenet_label(image_probs, decode_predictions)[1], get_imagenet_label(image_probs, decode_predictions)[2], example_categorie==get_imagenet_label(image_probs, decode_predictions)[1]]
    df_length = len(xception_df_fgsm)
    xception_df_fgsm.loc[df_length] = row
    # Creación de los ejemplos adversarios y prediccion de los mismos
    etiqueta_categoria = np.where(image_probs == image_probs.max())[1][0]
    label = tf.one_hot(etiqueta_categoria, image_probs.shape[-1])
    label = tf.reshape(label, (1, image_probs.shape[-1]))
    perturbations = create_adversarial_pattern_xception(image, label)
    for i, eps in enumerate(epsilons):
      adv_x = image + eps*perturbations
      adv_x = tf.clip_by_value(adv_x, -1, 1)
      ad_example_probs = xception_model.predict(adv_x)
      row = [file, "Adversarial Example", eps, example_categorie, get_imagenet_label(ad_example_probs, decode_predictions)[1], get_imagenet_label(ad_example_probs, decode_predictions)[2], example_categorie == get_imagenet_label(ad_example_probs, decode_predictions)[1]]
      df_length = len(xception_df_fgsm)
      xception_df_fgsm.loc[df_length] = row



# Predicciones para Red ResNet50
# Modelos y dataframe
resnet50_model = tf.keras.applications.ResNet50(include_top=True, weights='imagenet')
resnet50_model.trainable = False
resnet50_df_fgsm = pd.DataFrame(columns=["File", "Image Type", "Epsilon", "Category", "Predicted Label", "Accuracy", "Correct Prediction"])
decode_predictions = tf.keras.applications.resnet50.decode_predictions

def create_adversarial_pattern_resnet50(input_image, input_label):
  with tf.GradientTape() as tape:
    tape.watch(input_image)
    prediction = resnet50_model(input_image)
    loss = loss_object(input_label, prediction)
  # Get the gradients of the loss w.r.t to the input image.
  gradient = tape.gradient(loss, input_image)
  # Get the sign of the gradients to create the perturbation
  signed_grad = tf.sign(gradient)
  return signed_grad

# Bucle para todas las imágenes de las categorias de adexamples_categories
for example_categorie in adexamples_categories:
  for file in sorted(os.listdir(directorio+example_categorie)):
    # Selección de la imagen original
    image_path = directorio+example_categorie+"\\"+file
    image_raw = tf.io.read_file(image_path)
    image = tf.image.decode_image(image_raw)
    image = preprocess_resnet50(image)
    # Prediccion del modelo
    image_probs = resnet50_model.predict(image)
    row = [file, "Original", "", example_categorie, get_imagenet_label(image_probs, decode_predictions)[1], get_imagenet_label(image_probs, decode_predictions)[2], example_categorie==get_imagenet_label(image_probs, decode_predictions)[1]]
    df_length = len(resnet50_df_fgsm)
    resnet50_df_fgsm.loc[df_length] = row
    # Creación de los ejemplos adversarios y prediccion de los mismos
    etiqueta_categoria = np.where(image_probs == image_probs.max())[1][0]
    label = tf.one_hot(etiqueta_categoria, image_probs.shape[-1])
    label = tf.reshape(label, (1, image_probs.shape[-1]))
    perturbations = create_adversarial_pattern_resnet50(image, label)
    for i, eps in enumerate(epsilons):
      adv_x = image + eps*perturbations
      adv_x = tf.clip_by_value(adv_x, -1, 1)
      ad_example_probs = resnet50_model.predict(adv_x)
      row = [file, "Adversarial Example", eps, example_categorie, get_imagenet_label(ad_example_probs, decode_predictions)[1], get_imagenet_label(ad_example_probs, decode_predictions)[2], example_categorie == get_imagenet_label(ad_example_probs, decode_predictions)[1]]
      df_length = len(resnet50_df_fgsm)
      resnet50_df_fgsm.loc[df_length] = row


# Guardado de los dataframes de resultados
# mobilenetv2_df_fgsm.to_csv('dataframes_results\\mobilenetv2_df_fgsm.csv', index=False)
# inceptionv3_df_fgsm.to_csv('dataframes_results\\inceptionv3_df_fgsm.csv', index=False)
# xception_df_fgsm.to_csv('dataframes_results\\xception_df_fgsm.csv', index=False)
# resnet50_df_fgsm.to_csv('dataframes_results\\resnet50_df_fgsm.csv', index=False)
