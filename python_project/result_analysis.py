import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Lasso
import matplotlib.pyplot as plt
import seaborn as sns

# Por si se prefiere ejecutar los códigos de nuevo
# from FGSM_code import mobilenetv2_df_fgsm, inceptionv3_df_fgsm, xception_df_fgsm, resnet50_df_fgsm
# from CarliniWagner_code import mobilenetv2_df_carwag, inceptionv3_df_carwag, xception_df_carwag, resnet50_df_carwag
# from LinfPGD_code import mobilenetv2_df_lpgd, inceptionv3_df_lpgd, xception_df_lpgd, resnet50_df_lpgd

def calculateVIF(data):
    features = list(data.columns)
    num_features = len(features)

    model = LinearRegression()

    result = pd.DataFrame(index=['VIF'], columns=features)
    result = result.fillna(0)

    for ite in range(num_features):
        x_features = features[:]
        y_featue = features[ite]
        x_features.remove(y_featue)

        x = data[x_features]
        y = data[y_featue]

        model.fit(data[x_features], data[y_featue])

        result[y_featue] = 1 / (1 - model.score(data[x_features], data[y_featue]))

    return result


# Resultados de metodo FGSM
mobilenetv2_df_fgsm = pd.read_csv('dataframes_results\\mobilenetv2_df_fgsm.csv')
mobilenetv2_df_fgsm['network'] = 'mobilenetv2'
mobilenetv2_df_fgsm['method'] = 'fgsm'
inceptionv3_df_fgsm = pd.read_csv('dataframes_results\\inceptionv3_df_fgsm.csv')
inceptionv3_df_fgsm['network'] = 'inceptionv3'
inceptionv3_df_fgsm['method'] = 'fgsm'
xception_df_fgsm = pd.read_csv('dataframes_results\\xception_df_fgsm.csv')
xception_df_fgsm['network'] = 'xception'
xception_df_fgsm['method'] = 'fgsm'
resnet50_df_fgsm = pd.read_csv('dataframes_results\\resnet50_df_fgsm.csv')
resnet50_df_fgsm['network'] = 'resnet50'
resnet50_df_fgsm['method'] = 'fgsm'
# Resultados de metodo CarliniWagner
mobilenetv2_df_carwag = pd.read_csv('dataframes_results\\mobilenetv2_df_carwag.csv')
mobilenetv2_df_carwag['network'] = 'mobilenetv2'
mobilenetv2_df_carwag['method'] = 'carlini_wagner'
inceptionv3_df_carwag = pd.read_csv('dataframes_results\\inceptionv3_df_carwag.csv')
inceptionv3_df_carwag['network'] = 'inceptionv3'
inceptionv3_df_carwag['method'] = 'carlini_wagner'
xception_df_carwag = pd.read_csv('dataframes_results\\xception_df_carwag.csv')
xception_df_carwag['network'] = 'xception'
xception_df_carwag['method'] = 'carlini_wagner'
resnet50_df_carwag = pd.read_csv('dataframes_results\\resnet50_df_carwag.csv')
resnet50_df_carwag['network'] = 'resnet50'
resnet50_df_carwag['method'] = 'carlini_wagner'
# Resultados de metodo LinfPGD
mobilenetv2_df_lpgd = pd.read_csv('dataframes_results\\mobilenetv2_df_lpgd.csv')
mobilenetv2_df_lpgd['network'] = 'mobilenetv2'
mobilenetv2_df_lpgd['method'] = 'lpgd'
inceptionv3_df_lpgd = pd.read_csv('dataframes_results\\inceptionv3_df_lpgd.csv')
inceptionv3_df_lpgd['network'] = 'inceptionv3'
inceptionv3_df_lpgd['method'] = 'lpgd'
xception_df_lpgd = pd.read_csv('dataframes_results\\xception_df_lpgd.csv')
xception_df_lpgd['network'] = 'xception'
xception_df_lpgd['method'] = 'lpgd'
resnet50_df_lpgd = pd.read_csv('dataframes_results\\resnet50_df_lpgd.csv')
resnet50_df_lpgd['network'] = 'resnet50'
resnet50_df_lpgd['method'] = 'lpgd'

# Unimos todos los dataframes en uno solo
res_df = mobilenetv2_df_fgsm.append(inceptionv3_df_fgsm).append(xception_df_fgsm).append(resnet50_df_fgsm).append(mobilenetv2_df_carwag).append(inceptionv3_df_carwag).append(xception_df_carwag).append(resnet50_df_carwag).append(mobilenetv2_df_lpgd).append(inceptionv3_df_lpgd).append(xception_df_lpgd).append(resnet50_df_lpgd)
res_df.reset_index(inplace=True)
res_df.drop('index', axis=1, inplace=True)
# Liberamos memoria
del mobilenetv2_df_fgsm, inceptionv3_df_fgsm, xception_df_fgsm, resnet50_df_fgsm, mobilenetv2_df_carwag, inceptionv3_df_carwag, xception_df_carwag, resnet50_df_carwag, mobilenetv2_df_lpgd, inceptionv3_df_lpgd, xception_df_lpgd, resnet50_df_lpgd

# Aciertos de Imagenes Originales por cada una de las redes
aciertos_originales = res_df[res_df['Image Type'] == 'Original']
aciertos_originales_groupby_network = aciertos_originales.groupby('network').agg({'Correct Prediction':'mean'})

# Creamos un nuevo dataframe con las filas en las cuales la red haya acertado la imagen original, y también las filas de los ejemplos adversarios relacionadas con ese acierto de la imagen original.
new_df = pd.DataFrame(columns=["File", "Image Type", "Epsilon", "Category", "Predicted Label", "Accuracy", "Correct Prediction", "network", "method"])
for index, row in res_df.iterrows():
    if (row['Image Type'] == 'Original') & (row['Correct Prediction'] == True):
        new_df = new_df.append(res_df.iloc[index,:]).append(res_df.iloc[index+1,:]).append(res_df.iloc[index+2,:]).append(res_df.iloc[index+3,:]).append(res_df.iloc[index+4,:])
res_df = new_df
del new_df
#Una vez hecho eso eliminamos las filas de las predicciones de las imagenes originales quedándonos solo con los ejemplos adversarios
res_df_advs = res_df[res_df['Image Type'] == 'Adversarial Example']
# Por un lado habría que analizar las proporciones de fallos de cada uno de los métodos y redes
groupby_epsilon = res_df_advs.groupby('Epsilon').agg({'Correct Prediction':'mean'})
groupby_category = res_df_advs.groupby('Category').agg({'Correct Prediction':'mean'})
groupby_network = res_df_advs.groupby('network').agg({'Correct Prediction':'mean'})
groupby_method = res_df_advs.groupby('method').agg({'Correct Prediction':'mean'})
groupby_method.reset_index(inplace=True)

# Proporcion de aciertos por método
sns.set(style="darkgrid")
sns.set_palette(palette="summer", n_colors=3)
plt.figure()
ax = sns.barplot(x="method", y="Correct Prediction", data=groupby_method.round(2))
ax.set(xlabel="Método", ylabel="Proporción de aciertos")
ax.bar_label(ax.containers[0])

# Se observa que el método Carlini Wagner no ha generado correctamente los ejemplos adversarios ya que tiene una proporción de aciertos, de imágenes clasificadas correctamente de 1. Por tanto eliminamos este metodo de los siguientes análisis.
res_df = res_df[res_df['method']!='carlini_wagner']
res_df.reset_index(inplace=True)
res_df.drop('index', axis=1, inplace=True)
res_df_advs = res_df[res_df['Image Type'] == 'Adversarial Example']
res_df_advs.reset_index(inplace=True)
res_df_advs.drop('index', axis=1, inplace=True)





# Repetimos el análisis de las proporciones de aciertos habiendo eliminado el método de Carlini Wagner
groupby_all = res_df_advs.groupby(['Epsilon', 'Category', 'network', 'method']).agg({'Correct Prediction':'mean'})
groupby_all = groupby_all.reset_index()
groupby_epsilon = res_df_advs.groupby('Epsilon').agg({'Correct Prediction':'mean'})
groupby_epsilon.reset_index(inplace=True)
groupby_epsilon = groupby_epsilon.astype({"Epsilon":object})
groupby_category = res_df_advs.groupby('Category').agg({'Correct Prediction':'mean'})
groupby_category.reset_index(inplace=True)
groupby_network = res_df_advs.groupby('network').agg({'Correct Prediction':'mean'})
groupby_network.reset_index(inplace=True)
groupby_method = res_df_advs.groupby('method').agg({'Correct Prediction':'mean'})
groupby_method.reset_index(inplace=True)

# Proporcion de aciertos por método
sns.set(style="darkgrid")
sns.set_palette(palette="summer", n_colors=3)
plt.figure()
ax = sns.barplot(x="method", y="Correct Prediction", data=groupby_method.round(2))
ax.set(xlabel="Método", ylabel="Proporción de aciertos")
ax.bar_label(ax.containers[0])
# Proporcion de aciertos por red
sns.set(style="darkgrid")
sns.set_palette(palette="pink", n_colors=4)
plt.figure()
ax = sns.barplot(x="network", y="Correct Prediction", data=groupby_network.round(2))
ax.set(xlabel="Red Neuronal", ylabel="Proporción de aciertos")
ax.bar_label(ax.containers[0])
# Proporcion de aciertos por Categoría de imagen
sns.set(style="darkgrid")
sns.set_palette(palette="YlGnBu", n_colors=4)
plt.figure()
ax = sns.barplot(x="Category", y="Correct Prediction", data=groupby_category.round(2))
ax.set(xlabel="Categoría Imagen", ylabel="Proporción de aciertos")
ax.bar_label(ax.containers[0])
# Proporcion de aciertos por epsilon
sns.set(style="darkgrid")
sns.set_palette(palette="copper", n_colors=4)
plt.figure()
ax = sns.barplot(x="Epsilon", y="Correct Prediction", data=groupby_epsilon.round(2))
ax.set(xlabel="Epsilon", ylabel="Proporción de aciertos")
ax.bar_label(ax.containers[0])

# Conversion de vv categoricas a dummies para la regresión de proporción de aciertos como VD
vv_cat = (groupby_all.dtypes == 'object')
vv_cat = list(vv_cat[vv_cat==True].index)
groupby_all_vvcat = groupby_all[vv_cat]
for column in list(groupby_all_vvcat.columns):
    dummie = pd.get_dummies(groupby_all_vvcat[column], prefix=column)
    names = list(dummie.columns)
    names.remove(names[0])
    groupby_all_vvcat = pd.concat([groupby_all_vvcat, dummie[names]], axis=1)
    names = list(groupby_all_vvcat.columns)
    names.remove(column)
    groupby_all_vvcat = groupby_all_vvcat[names]
groupby_all_regress = pd.concat([groupby_all, groupby_all_vvcat], axis=1)
groupby_all_regress = groupby_all_regress[['Epsilon', 'Category_electric_guitar', 'Category_flamingo', 'Category_schooner', 'network_mobilenetv2', 'network_resnet50', 'network_xception', 'method_lpgd', 'Correct Prediction']]
# Análisis VIF para comprobar multicolinealidad
vif_groupby_all_regress = calculateVIF(groupby_all_regress)
vif_groupby_all_regress > 5
# Regresión Lineal
groupby_all_regress_x = groupby_all_regress.loc[:, groupby_all_regress.columns != 'Correct Prediction']
groupby_all_regress_y = groupby_all_regress[['Correct Prediction']]
model = LinearRegression()
model.fit(groupby_all_regress_x, groupby_all_regress_y)
coefs = [model.intercept_[0].tolist()] + list(model.coef_[0])
coefs_names = ['Intercept'] + list(groupby_all_regress_x.columns)
coefs_df_prop = pd.DataFrame(coefs, coefs_names)
r2_prop = model.score(groupby_all_regress_x, groupby_all_regress_y)
model = Lasso(alpha=0.001)
model.fit(groupby_all_regress_x, groupby_all_regress_y)
coefs = list(model.coef_)
coefs_names = list(groupby_all_regress_x.columns)
coefs_lasso_df_prop = pd.DataFrame(coefs, coefs_names)
r2_lasso_prop = model.score(groupby_all_regress_x, groupby_all_regress_y)





# Por otro regresión del accuracy de los ejemplos mal clasificados
res_df_advs_missclass = res_df_advs[res_df_advs['Correct Prediction']==False]
res_df_advs_missclass = res_df_advs_missclass[['Epsilon', 'Category', 'network', 'method', 'Accuracy']]
# Descriptivos
missclass_groupby_epsilon = res_df_advs_missclass.groupby('Epsilon').agg({'Accuracy':'mean'})
missclass_groupby_epsilon.reset_index(inplace=True)
missclass_groupby_epsilon = missclass_groupby_epsilon.astype({"Epsilon":object})
missclass_groupby_category = res_df_advs_missclass.groupby('Category').agg({'Accuracy':'mean'})
missclass_groupby_category.reset_index(inplace=True)
missclass_groupby_network = res_df_advs_missclass.groupby('network').agg({'Accuracy':'mean'})
missclass_groupby_network.reset_index(inplace=True)
missclass_groupby_method = res_df_advs_missclass.groupby('method').agg({'Accuracy':'mean'})
missclass_groupby_method.reset_index(inplace=True)

# Media del Accuracy por método
sns.set(style="darkgrid")
sns.set_palette(palette="summer", n_colors=3)
plt.figure()
ax = sns.barplot(x="method", y="Accuracy", data=missclass_groupby_method.round(2))
ax.set(xlabel="Método", ylabel="Accuracy")
ax.bar_label(ax.containers[0])
# Media del Accuracy por red
sns.set(style="darkgrid")
sns.set_palette(palette="pink", n_colors=4)
plt.figure()
ax = sns.barplot(x="network", y="Accuracy", data=missclass_groupby_network.round(2))
ax.set(xlabel="Red Neuronal", ylabel="Accuracy")
ax.bar_label(ax.containers[0])
# Media del Accuracy por Categoría de imagen
sns.set(style="darkgrid")
sns.set_palette(palette="YlGnBu", n_colors=4)
plt.figure()
ax = sns.barplot(x="Category", y="Accuracy", data=missclass_groupby_category.round(2))
ax.set(xlabel="Categoría Imagen", ylabel="Accuracy")
ax.bar_label(ax.containers[0])
# Media del Accuracy por epsilon
sns.set(style="darkgrid")
sns.set_palette(palette="copper", n_colors=4)
plt.figure()
ax = sns.barplot(x="Epsilon", y="Accuracy", data=missclass_groupby_epsilon.round(2))
ax.set(xlabel="Epsilon", ylabel="Accuracy")
ax.bar_label(ax.containers[0])


# Conversion de vv categoricas a dummies para la regresión
vv_cat = (res_df_advs_missclass.dtypes == 'object')
vv_cat = list(vv_cat[vv_cat==True].index)
res_df_advs_missclass_vvcat = res_df_advs_missclass[vv_cat]
for column in list(res_df_advs_missclass_vvcat.columns):
    dummie = pd.get_dummies(res_df_advs_missclass_vvcat[column], prefix=column)
    names = list(dummie.columns)
    names.remove(names[0])
    res_df_advs_missclass_vvcat = pd.concat([res_df_advs_missclass_vvcat, dummie[names]], axis=1)
    names = list(res_df_advs_missclass_vvcat.columns)
    names.remove(column)
    res_df_advs_missclass_vvcat = res_df_advs_missclass_vvcat[names]
res_df_advs_missclass_regress = pd.concat([res_df_advs_missclass, res_df_advs_missclass_vvcat], axis=1)
res_df_advs_missclass_regress = res_df_advs_missclass_regress[['Epsilon', 'Category_electric_guitar', 'Category_flamingo', 'Category_schooner', 'network_mobilenetv2', 'network_resnet50', 'network_xception', 'method_lpgd', 'Accuracy']]
# Análisis VIF para comprobar multicolinealidad
vif_res_df_advs_missclass_regress = calculateVIF(res_df_advs_missclass_regress)
vif_res_df_advs_missclass_regress > 5
# Regresión Lineal
res_df_advs_missclass_regress_x = res_df_advs_missclass_regress.loc[:, res_df_advs_missclass_regress.columns != 'Accuracy']
res_df_advs_missclass_regress_y = res_df_advs_missclass_regress[['Accuracy']]
model = LinearRegression()
model.fit(res_df_advs_missclass_regress_x, res_df_advs_missclass_regress_y)
coefs = [model.intercept_[0].tolist()] + list(model.coef_[0])
coefs_names = ['Intercept'] + list(res_df_advs_missclass_regress_x.columns)
coefs_df_miss = pd.DataFrame(coefs, coefs_names)
r2_miss = model.score(res_df_advs_missclass_regress_x, res_df_advs_missclass_regress_y)
model = Lasso(alpha=0.001)
model.fit(res_df_advs_missclass_regress_x, res_df_advs_missclass_regress_y)
coefs = list(model.coef_)
coefs_names = list(res_df_advs_missclass_regress_x.columns)
coefs_lasso_df_miss = pd.DataFrame(coefs, coefs_names)
r2_lasso_miss = model.score(res_df_advs_missclass_regress_x, res_df_advs_missclass_regress_y)



# Por otro regresión del accuracy de los ejemplos correctamente clasificados añadiendo los originales
res_df_wellclass = res_df[res_df['Correct Prediction']==True]
res_df_wellclass['Epsilon'] = res_df_wellclass['Epsilon'].fillna(0)
res_df_wellclass = res_df_wellclass[['Image Type', 'Epsilon', 'Category', 'network', 'method', 'Accuracy']]
wellclass_groupby_imagetype = res_df_wellclass.groupby('Image Type').agg({'Accuracy':'mean'})
wellclass_groupby_imagetype.reset_index(inplace=True)

res_df_advs_wellclass = res_df_wellclass[res_df_wellclass['Image Type'] == 'Adversarial Example']
wellclass_groupby_epsilon = res_df_advs_wellclass.groupby('Epsilon').agg({'Accuracy':'mean'})
wellclass_groupby_epsilon.reset_index(inplace=True)
wellclass_groupby_epsilon = wellclass_groupby_epsilon.astype({"Epsilon":object})
wellclass_groupby_category = res_df_advs_wellclass.groupby('Category').agg({'Accuracy':'mean'})
wellclass_groupby_category.reset_index(inplace=True)
wellclass_groupby_network = res_df_advs_wellclass.groupby('network').agg({'Accuracy':'mean'})
wellclass_groupby_network.reset_index(inplace=True)
wellclass_groupby_method = res_df_advs_wellclass.groupby('method').agg({'Accuracy':'mean'})
wellclass_groupby_method.reset_index(inplace=True)

# Media del Accuracy por método
sns.set(style="darkgrid")
sns.set_palette(palette="summer", n_colors=3)
plt.figure()
ax = sns.barplot(x="method", y="Accuracy", data=wellclass_groupby_method.round(2))
ax.set(xlabel="Método", ylabel="Accuracy")
ax.bar_label(ax.containers[0])
# Media del Accuracy por red
sns.set(style="darkgrid")
sns.set_palette(palette="pink", n_colors=4)
plt.figure()
ax = sns.barplot(x="network", y="Accuracy", data=wellclass_groupby_network.round(2))
ax.set(xlabel="Red Neuronal", ylabel="Accuracy")
ax.bar_label(ax.containers[0])
# Media del Accuracy por Categoría de imagen
sns.set(style="darkgrid")
sns.set_palette(palette="YlGnBu", n_colors=4)
plt.figure()
ax = sns.barplot(x="Category", y="Accuracy", data=wellclass_groupby_category.round(2))
ax.set(xlabel="Categoría Imagen", ylabel="Accuracy")
ax.bar_label(ax.containers[0])
# Media del Accuracy por epsilon
sns.set(style="darkgrid")
sns.set_palette(palette="copper", n_colors=4)
plt.figure()
ax = sns.barplot(x="Epsilon", y="Accuracy", data=wellclass_groupby_epsilon.round(2))
ax.set(xlabel="Epsilon", ylabel="Accuracy")
ax.bar_label(ax.containers[0])
# Media del Accuracy por Tipo de imagen
sns.set(style="darkgrid")
sns.set_palette(palette="YlOrBr", n_colors=2)
plt.figure()
ax = sns.barplot(x="Image Type", y="Accuracy", data=wellclass_groupby_imagetype.round(2))
ax.set(xlabel="Tipo de Imagen", ylabel="Accuracy")
ax.bar_label(ax.containers[0])


# Conversion de vv categoricas a dummies para la regresión
vv_cat = (res_df_wellclass.dtypes == 'object')
vv_cat = list(vv_cat[vv_cat==True].index)
res_df_wellclass_vvcat = res_df_wellclass[vv_cat]
for column in list(res_df_wellclass_vvcat.columns):
    dummie = pd.get_dummies(res_df_wellclass_vvcat[column], prefix=column)
    names = list(dummie.columns)
    names.remove(names[0])
    res_df_wellclass_vvcat = pd.concat([res_df_wellclass_vvcat, dummie[names]], axis=1)
    names = list(res_df_wellclass_vvcat.columns)
    names.remove(column)
    res_df_wellclass_vvcat = res_df_wellclass_vvcat[names]
res_df_wellclass_regress = pd.concat([res_df_wellclass, res_df_wellclass_vvcat], axis=1)
res_df_wellclass_regress = res_df_wellclass_regress[['Epsilon', 'Image Type_Original', 'Category_electric_guitar', 'Category_flamingo', 'Category_schooner', 'network_mobilenetv2', 'network_resnet50', 'network_xception', 'method_lpgd', 'Accuracy']]
# Análisis VIF para comprobar multicolinealidad
vif_res_df_wellclass_regress = calculateVIF(res_df_wellclass_regress)
vif_res_df_wellclass_regress > 5
# Regresión Lineal
res_df_wellclass_regress_x = res_df_wellclass_regress.loc[:, res_df_wellclass_regress.columns != 'Accuracy']
res_df_wellclass_regress_y = res_df_wellclass_regress[['Accuracy']]
model = LinearRegression()
model.fit(res_df_wellclass_regress_x, res_df_wellclass_regress_y)
coefs = [model.intercept_[0].tolist()] + list(model.coef_[0])
coefs_names = ['Intercept'] + list(res_df_wellclass_regress_x.columns)
coefs_df_well = pd.DataFrame(coefs, coefs_names)
r2_well = model.score(res_df_wellclass_regress_x, res_df_wellclass_regress_y)
model = Lasso(alpha=0.001)
model.fit(res_df_wellclass_regress_x, res_df_wellclass_regress_y)
coefs = list(model.coef_)
coefs_names = list(res_df_wellclass_regress_x.columns)
coefs_lasso_df_well = pd.DataFrame(coefs, coefs_names)
r2_lasso_well = model.score(res_df_wellclass_regress_x, res_df_wellclass_regress_y)



